<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReadThreadsTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();
        $this->thread = factory('App\Thread')->create();
    }

    /** @test */
    public function a_user_can_view_all_threads()
    {
        $this->get('/threads')->assertSee($this->thread->title);
    }

    /** @test */
    public function a_user_can_read_a_single_thread()
    {
        $this->get($this->thread->path())->assertSee($this->thread->title);
    }

    /** @test */
    public function a_user_can_filter_threads_according_to_a_channel()
    {
        $channel = create('App\Channel');
        $threadInChannel = create('App\Thread', ['channel_id' => $channel->id]);
        $threadNotInChannel = create('App\Thread');

        $this->get('/threads/' . $channel->slug)
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel->title);
    }

    /** @test */
    public function a_user_can_filter_threads_by_those_that_are_unanswered()
    {
        $thread = create('App\Thread');
        create('App\Reply', ['thread_id' => $thread->id]);
        $response = $this->getJson('threads?unanswered=1')->json();
        $this->assertCount(1, $response);
    }

    /** @test */
    public function a_user_can_filter_threads_by_any_username()
    {
        $this->signIn(create('App\User', ['name' => 'JohnDoe']));
        $threadByJohn = create('App\Thread', ['user_id' => auth()->id()]);
        $threadNotByJohn = create('App\Thread');
        $this->get('threads?by=JohnDoe')
            ->assertSee($threadByJohn->title)
            ->assertDontSee($threadNotByJohn->title);
    }

    /** @test */
    public function a_user_can_filter_threads_by_popularity()
    {
        $threadWithTwoReplies = create('App\Thread');
        create('App\Reply', ['thread_id' => $threadWithTwoReplies->id], 2);

        $threadWithThreeReplies = create('App\Thread');
        create('App\Reply', ['thread_id' => $threadWithThreeReplies->id], 3);

        $threadWithNoReplies = $this->thread;

        $response = $this->getJson('threads?popular=1');
        $this->assertEquals([3, 2, 0], collect($response->json())->pluck('replies_count')->toArray());
    }

    /** @test */
    public function a_user_can_filter_threads_and_still_secondary_sort_by_time()
    {
        $firstThread = create('App\Thread', ['title' => 'Title 1', 'created_at' => new \Carbon\Carbon('-1 minute')]);
        create('App\Reply', ['thread_id' => $firstThread->id], 3);

        $secondThread = create('App\Thread', ['title' => 'Title 2', 'created_at' => new \Carbon\Carbon('-2 minute')]);
        create('App\Reply', ['thread_id' => $secondThread->id], 3);

        $response = $this->getJson(action('ThreadsController@index') . '?popular=1')->json();

        $this->assertEquals(['Title 1', 'Title 2'], collect($response)->take(2)->pluck('title')->toArray());
    }

    /** @test */
    public function a_user_can_request_all_replies_for_a_given_thread()
    {
        create('App\Reply', ['thread_id' => $this->thread->id], 2);

        $response = $this->getJson($this->thread->path() . '/replies')->json();

        $this->assertCount(2, $response['data']);
        $this->assertEquals(2, $response['total']);
    }
}
