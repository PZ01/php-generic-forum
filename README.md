# README #

I wanted to try a new bleeding edge stack. Linux + NGinX + MySQL + PHP 7.1 with VueJS 2.1 and Webpack for scaffolding. 
The forum supports EcmaScript 6 and SASS compilation into .JS and .CSS files for dev and prod.

### What is this repository for? ###

* Using cool technologies on a rather complex project(being a forum where users can post and reply to threads... etc).

### How do I get set up? ###

* [Learn Laravel Installation](https://laravel.com/docs/5.6/installation)

### Contribution guidelines ###

* Unit tests are powered by PHP Unit. Simply run it from the cmd line.
